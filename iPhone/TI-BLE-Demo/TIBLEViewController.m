//
//  TIBLEViewController.m
//  TI-BLE-Demo
//
//  Created by Ole Andreas Torvmark on 10/29/11.
//  Copyright (c) 2011 ST alliance AS. All rights reserved.
//

#import "TIBLEViewController.h"

@implementation TIBLEViewController
@synthesize TIBLEUIAccelXBar;
@synthesize TIBLEUIAccelYBar;
@synthesize TIBLEUIAccelZBar;
@synthesize TIBLEUILeftButton;
@synthesize TIBLEUIRightButton;
@synthesize TIBLEUISpinner;
@synthesize TIBLEUITextView;
@synthesize TIBLEUIBytes;
@synthesize TIBLEUIFileCounter;



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    t = [[TIBLECBKeyfob alloc] init];   // Init TIBLECBKeyfob class.
    [t controlSetup:1];                 // Do initial setup of TIBLECBKeyfob class.
    t.delegate = self;                  // Set TIBLECBKeyfob delegate class to point at methods implemented in this class.
    
    // Do any additional setup after loading the view, typically from a nib.
    NSFileManager *filemgr;
    NSString *dataFile;
    NSString *docsDir;
    NSArray *dirPaths;
    
    filemgr = [NSFileManager defaultManager];
    
    // Identify the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(
                                                   NSDocumentDirectory, NSUserDomainMask, YES);
    
    docsDir = [dirPaths objectAtIndex:0];
    
    // Build the path to the data file
    dataFile = [docsDir stringByAppendingPathComponent: 
                @"datafile.dat"];
    
    // Check if the file already exists
    if ([filemgr fileExistsAtPath: dataFile])
    {
        // Read file contents and display in textBox
        NSData *databuffer;
        databuffer = [filemgr contentsAtPath: dataFile];
        
        NSString *datastring = [[NSString alloc] 
                                initWithData: databuffer 
                                encoding:NSASCIIStringEncoding];
        
         TIBLEUITextView.text = datastring;
    }
}

- (void)viewDidUnload
{
    [self setTIBLEUIAccelXBar:nil];
    [self setTIBLEUIAccelYBar:nil];
    [self setTIBLEUIAccelZBar:nil];
    [self setTIBLEUILeftButton:nil];
    [self setTIBLEUIRightButton:nil];
    [self setTIBLEUISpinner:nil];
    [self setTIBLEUITextView:nil];
    [self setTIBLEUIFileCounter:nil];
    [self setTIBLEUITextView:nil];
    [self setTIBLEUIBytes:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
        switch(interfaceOrientation)
        {
            case UIInterfaceOrientationLandscapeLeft:
                return NO;
            case UIInterfaceOrientationLandscapeRight:
                return NO;
            default:
                return YES;
        }
}

- (IBAction)TIBLEUIScanForPeripheralsButton:(id)sender {
    if (t.activePeripheral) if(t.activePeripheral.isConnected) [[t CM] cancelPeripheralConnection:[t activePeripheral]];
    if (t.peripherals) t.peripherals = nil;
    [t findBLEPeripherals:2];   
    [NSTimer scheduledTimerWithTimeInterval:(float)2.0 target:self selector:@selector(connectionTimer:) userInfo:nil repeats:NO];
    [TIBLEUISpinner startAnimating];
    TIBLEUITextView.text = @"scanning";
    fileCounter = 0;
}


// Method from TIBLECBKeyfobDelegate, called when accelerometer values are updated
-(void) accelerometerValuesUpdated:(char)x y:(char)y z:(char)z {
    TIBLEUIAccelXBar.progress = (float)(x + 50) / 100;
    TIBLEUIAccelYBar.progress = (float)(y + 50) / 100;
    TIBLEUIAccelZBar.progress = (float)(z + 50) / 100;
}
// Method from TIBLECBKeyfobDelegate, called when key values are updated
-(void) keyValuesUpdated:(char)sw {
    printf("Key values updated ! \r\n");
    if (sw & 0x1) [TIBLEUILeftButton setOn:TRUE];
    else [TIBLEUILeftButton setOn: FALSE];
    if (sw & 0x2) [TIBLEUIRightButton setOn: TRUE];
    else [TIBLEUIRightButton setOn: FALSE];
    
}

// Method from TIBLECBKeyfobDelegate, called when key values are updated
-(void) textValuesUpdated:(NSString*)txt {

    fileCounter++;
    fileBytes = fileCounter * 20;
    TIBLEUIFileCounter.text = [NSString stringWithFormat: @"%d", fileCounter];
    TIBLEUIBytes.text = [NSString stringWithFormat: @"%d", fileBytes];

    
    static NSString *keyFobLog = @"log\n";
    
    //keyFobLog = [keyFobLog stringByAppendingString:@"123456789abcdefghijk/n"];
    keyFobLog = [keyFobLog stringByAppendingString:txt];

    NSLog(@"keyFobLog = %@", keyFobLog);

    //save to file
    if(fileCounter == 2000)
    {
        
        NSFileManager *filemgr;
        NSData *databuffer;
        NSString *dataFile;
        NSString *docsDir;
        NSArray *dirPaths;
        
        filemgr = [NSFileManager defaultManager];
        
        dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        
        docsDir = [dirPaths objectAtIndex:0];
        dataFile = [docsDir stringByAppendingPathComponent: @"datafile.dat"];
        
        databuffer = [keyFobLog dataUsingEncoding: NSASCIIStringEncoding];
        [filemgr createFileAtPath: dataFile 
                         contents: databuffer attributes:nil];        
        
        TIBLEUITextView.text = keyFobLog;
        
    }
}

//Method from TIBLECBKeyfobDelegate, called when keyfob has been found and all services have been discovered
-(void) keyfobReady {

    [t enableAccelerometer:[t activePeripheral]];   // Enable accelerometer (if found)
    [t enableTXPower:[t activePeripheral]];         // Enable TX power service (if found)
    [TIBLEUISpinner stopAnimating]; 
     TIBLEUITextView.text = @"Connected";
}

//Method from TIBLECBKeyfobDelegate, called when TX powerlevel values are updated
-(void) TXPwrLevelUpdated:(char)TXPwr {
}

// Called when scan period is over to connect to the first found peripheral
-(void) connectionTimer:(NSTimer *)timer {
    if(t.peripherals.count > 0)
    {
        [t connectPeripheral:[t.peripherals objectAtIndex:0]];

    }
    else 
    {
        [TIBLEUISpinner stopAnimating];
         TIBLEUITextView.text = @"stopped";
    }
}

- (IBAction)TIBLEUISoundBuzzerButton:(id)sender {
    [t soundBuzzer:0x02 p:[t activePeripheral]]; //Sound buzzer with 0x02 as data value
}

- (IBAction)TIBLEUIClear:(id)sender {
   TIBLEUITextView.text = @"log";
    fileCounter = 0;
    
    
}



    






@end
